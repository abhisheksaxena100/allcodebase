package com.abhi.files;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
//        import java.util.Map;

public class TestAllFilesAttributes {

    private static final String TARGET_FOLDER = "F:\\Target Folder";



    private static final String SOURCE_FOLDER = "C:\\Collect Videos\\Phone-BKP-09-May-2020";
    //    private static final String SOURCE_FOLDER = "E:\\Pics---\\Pics_70GB-Org-Copy\\3.5";
   // private static final String SOURCE_FOLDER = "E:\\Pics---\\Pics_70GB-Org-Copy\\1";
   // private static final String SOURCE_FOLDER = "E:\\Pics---\\Pics_70GB-Org-Copy\\5800";
   // private static final String SOURCE_FOLDER = "E:\\Pics---\\Phone_BKP";
    //private static final String SOURCE_FOLDER = System.getProperty("user.dir");

    public static String logFileName = generateUniqueFileName();

    public static void main(String[] args) {

        try {

            String dir = SOURCE_FOLDER;
            System.out.println(" Log File Name = " + logFileName);
            logFileName = dir +"\\"+logFileName;
            System.out.println("current dir = " + dir);
            BufferedWriter writer = Files.newBufferedWriter(Paths.get(logFileName));
            writer.write("Strating writing in to the file "+logFileName);

            sortFiles(dir);

            // writer.close();
        }catch (IOException io) {
            System.out.println("<!-----File Not Found-----!>");
            System.err.format("IOException: %s%n", io);
            io.printStackTrace();
            //writer.close();
        }

/*
      BasicFileAttributes attr = Files.readAttributes(file, BasicFileAttributes.class);
      logDetailsToFile("creationTime: " + attr.creationTime());
      logDetailsToFile("lastAccessTime: " + attr.lastAccessTime());
      logDetailsToFile("lastModifiedTime: " + attr.lastModifiedTime());
      logDetailsToFile("isDirectory: " + attr.isDirectory());
      logDetailsToFile("isOther: " + attr.isOther());
      logDetailsToFile("isRegularFile: " + attr.isRegularFile());
      logDetailsToFile("isSymbolicLink: " + attr.isSymbolicLink());
      logDetailsToFile("size: " + attr.size());
 */

    }

    public static void sortFiles(String dir) throws IOException {
        File folder = new File(dir);
        File[] listOfFiles = folder.listFiles();
        //Path sourcePath, destPath;
        for (int i = 0; i < listOfFiles.length; i++) {
            File file = listOfFiles[i];
            logDetailsToFile(" File Name >>> " + file.getName()
                    + ", file.isFile() >>> " + file.isFile()
                    + ", file.isDirectory() >>> " + file.isDirectory()
            );

            if (file.isFile() &&
                    (
                            file.getName().endsWith(".jpg") || file.getName().endsWith(".JPG")
                                    || file.getName().endsWith(".jpeg") || file.getName().endsWith(".JPEG")
                                    || file.getName().endsWith(".png") || file.getName().endsWith(".PNG")
                    )
                    /*
                   file.getName().endsWith(".jpg") || file.getName().endsWith(".JPG")
 || file.getName().endsWith(".jpeg") || file.getName().endsWith(".JPEG")
 || file.getName().endsWith(".png") || file.getName().endsWith(".PNG")

                     file.getName().endsWith(".mp3") || file.getName().endsWith(".MP3")
 || file.getName().endsWith(".wma") || file.getName().endsWith(".WMA")
 || file.getName().endsWith(".wav") || file.getName().endsWith(".WAV")

  file.getName().endsWith(".mp4") || file.getName().endsWith(".MP4")
                           ||  file.getName().endsWith(".avi") || file.getName().endsWith(".AVI")
                                    || file.getName().endsWith(".mkv") || file.getName().endsWith(".MKV")
                                    || file.getName().endsWith(".wmv") || file.getName().endsWith(".WMV")
                     */
            ) {
                logDetailsToFile(" ***** Source Path >>> " + file.getPath());
                logDetailsToFile(" ***** Target Path >>> " + calculateDestinationPath(file));
                copyFile_Directory(file.getPath(), calculateDestinationPath(file));

                //sourcePath = Paths.get(file.getPath());
                //destPath = Paths.get(calculateDestinationPath(file));
                //Map<String, Object> attributes;
                //attributes = Files.readAttributes(sourcePath, "dos:*");
                //attributes = Files.readAttributes(sourcePath, "lastModifiedTime,lastAccessTime");
                //attributes.entrySet().forEach(entry ->  System.out.printf("%15s = %s%n", entry.getKey(), entry.getValue()));
            }
            else if(file.isDirectory()){
                sortFiles(file.getPath());
            }
        }
    }
    public static String calculateDestinationPath(File file) throws IOException {

        Path filePath = Paths.get(file.getPath());
        BasicFileAttributes attr = Files.readAttributes(filePath, BasicFileAttributes.class);

        logDetailsToFile("creationTime: " + attr.creationTime());
        logDetailsToFile("lastModifiedTime: " + attr.lastModifiedTime());

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(attr.lastModifiedTime().toMillis()));

        String[] fileAttrs = {
                "File Attributes for file"
                ,String.valueOf(file.getName())
                , ":\nYear is ->", String.valueOf(calendar.get(Calendar.YEAR))
                , "Month is  ->", calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault())
                , "Day is  ->", String.valueOf(calendar.get(Calendar.DAY_OF_MONTH))
                , "Minute is  ->",  String.valueOf(calendar.get(Calendar.MINUTE))
                , "Hour is  ->", String.valueOf(calendar.get(Calendar.HOUR))
                , "SECOND is  ->",  String.valueOf(calendar.get(Calendar.SECOND))
        };

        logDetailsToFile(String.join("  ", fileAttrs));

        String[] pathElements = {
                TARGET_FOLDER
                , String.valueOf(calendar.get(Calendar.YEAR))
                , String.valueOf(calendar.get(Calendar.MONTH)) + calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault())
                , String.valueOf(calendar.get(Calendar.DAY_OF_MONTH))
                , String.valueOf(calendar.get(Calendar.HOUR))
                , file.getName()
        };

        return String.join("\\", pathElements);
    }


    public static void copyFile_Directory(String origin, String destDir) throws  IOException {

        Path sourcePath = Paths.get(origin);
        Path destPath = Paths.get(destDir);

        logDetailsToFile("dir to create: " + destPath);
        logDetailsToFile("dir exits: " + Files.exists(destPath));
        //creating directories
        try {       Path directories = Files.createDirectories(destPath);
            logDetailsToFile("directories created: " + destPath);
            logDetailsToFile("dir created exits: " + Files.exists(destPath));

            //overwrite the destination file if it exists, and copy the file attributes, including the rwx permissions
            CopyOption[] options = new CopyOption[]{ StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES };

            Files.copy(sourcePath, destPath, options);
        } catch (java.nio.file.FileAlreadyExistsException e) {
            logDetailsToFile("Duplicate FIle ");
            return;
        }
    }

    public static void logDetailsToFile(String contentToAppend) throws  IOException {
        //System.out.println(" contentToAppend = " + contentToAppend);
        Files.write(Paths.get(logFileName), (contentToAppend+="\n").getBytes(), StandardOpenOption.APPEND);
    }

    public static String  generateUniqueFileName() {
        long millis = System.currentTimeMillis();
        Date date = new Date() ;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-ddHH-mm-ss") ;
        return dateFormat.format(date) + "_" + millis + ".log";
    }

}
